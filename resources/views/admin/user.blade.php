@extends('admin.layouts.main')

@section('header')
<a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block">USER</a>
@endsection

@section('content')
<div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Active User</h3>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">USERNAME</th>
                    <th scope="col">EMAIL</th>
                    <th scope="col">STATUS</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <th scope="row">
                                <div class="media align-items-center">
                                    
                                    <div class="media-body">
                                    <span class="mb-0 text-sm">{{$user->username}}</span>
                                    </div>
                                </div>
                            </th>
                            <td>
                            
                                {{$user->email}}
                            </td>
                            <td> <!-- STATUS -->
                            <div class="avatar-group">
                                
                                @switch($user->status)
                                    @case(-1)
                                        <button type="button" class="btn btn-danger btn-sm" border-radius="10px">Banned</button>
                                        @break
                                    @case(0)
                                        <button type="button" class="btn btn-warning btn-sm" border-radius="10px">Not Verified</button>
                                        @break
                                    @case(1)
                                        <button type="button" class="btn btn-success btn-sm" border-radius="10px">Verified</button>
                                        @break  
                                @endswitch
                                    
                                

                            </div>
                            </td>
                            <td> <!-- OPTIONS -->
                            <div class="d-flex align-items-center">
                            @switch($user->status)
                                    @case(-1)
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#unbanNow{{$user->id}}">Unbanned</button>
                                        @break
                                    @case(0)
                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#banNow{{$user->id}}" disabled>Banned</button>
                                        @break
                                    @case(1)
                                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#banNow{{$user->id}}">Banned</button>
                                        @break  
                                @endswitch
                                
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteNow{{$user->id}}">Delete</button>
                                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#detailNow{{$user->id}}">Detail</button>
                            </div>
                            </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            
          </div>
        </div> 

    
            

            @foreach($users as $user)
                <!-- Ban Data -->
                <div class="modal fade" id="banNow{{$user->id}}" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Edit Data</h4>
                            </div>
                            <form action="{{route('admin.user.updateStatus',[$user->id,$user->status])}}" method="POST">
                                <div class="modal-body">
                                    Are you sure want to banned <strong>{{$user->username}}?</strong>
                                    <div class="modal-footer">
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <input type="hidden" name="status" value="-1">
                                        <button type="submit" class="btn btn-link waves-effect">BAN</button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                                        @csrf
                                        <input type="hidden" name="_method" value="PUT">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach

             @foreach($users as $user)
                <!-- Ban Data -->
                <div class="modal fade" id="unbanNow{{$user->id}}" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Edit Data</h4>
                            </div>
                            <form action="{{route('admin.user.updateStatus',[$user->id,$user->status])}}" method="POST">
                                <div class="modal-body">
                                    Are you sure want to unbanned <strong>{{$user->username}}?</strong>
                                    <div class="modal-footer">
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <input type="hidden" name="status" value="1">
                                        <button type="submit" class="btn btn-link waves-effect">YES</button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                                        @csrf
                                        <input type="hidden" name="_method" value="PUT">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
@endsection


@section('detail')
    @foreach($users as $user)
        <!-- Detail Data -->
        <div class="modal fade" id="detailNow{{$user->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Detail</h4>
                        </div>
                        <div class="modal-body">

                            <form>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <h5>First Name</h5>
                                            <input disabled name="first_name" value="{{$user->first_name}}" class="form-control" placeholder="First Name" type="text">
                                        </div>
                                        <div class="form-line">
                                            <h5>Last Name</h5>
                                            <input disabled name="last_name" value="{{$user->last_name}}" class="form-control" placeholder="First Name" type="text">
                                        </div>
                                        <div class="form-line">
                                            <h5>Username</h5>
                                            <input disabled name="username" value="{{$user->username}}" class="form-control" placeholder="First Name" type="text">
                                        </div>
                                        <div class="form-line">
                                            <h5>Email</h5>
                                            <input disabled name="email" value="{{$user->email}}" class="form-control" placeholder="First Name" type="text">
                                        </div>
                                        <div class="form-line">
                                            <h5>Gender</h5>
                                            <select disabled name="sex" class="form-control" id="sel1">
                                                @switch($user->sex)
                                                    @case(1)
                                                    <option  value="1">Male</option>
                                                    <option value="2">Female</option>
                                                    @break
                                                    @case(2)
                                                    <option value="1">Male</option>
                                                    <option selected value="2">Female</option>
                                                    @break
                                                @endswitch
                                            </select>
                                        </div>
                                        <div class="form-line">
                                            <h5>Phone</h5>
                                            <input disabled name="phone_numb" value="{{$user->phone_numb}}" class="form-control" placeholder="First Name" type="text">
                                        </div>
                                    </div>
                                  
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="PUT">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </form>

                    </div>
                </div>
            </div>
    @endforeach
@endsection


@foreach($users as $user)
                <!-- Delete Data -->
                <div class="modal fade" id="deleteNow{{$user->id}}" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Edit Data</h4>
                            </div>
                            <form action="{{route('admin.user.destroy',[$user->id])}}" method="POST">
                                <div class="modal-body">
                                    Are you sure want to delete <strong>{{$user->username}}?</strong>
                                    <div class="modal-footer">
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <button type="submit" class="btn btn-link waves-effect">YES</button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach