@extends('layouts.main')

@section('header')
<a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block">MAPS</a>
@endsection


@section('content')
<div class="col">
          <div class="card shadow border-0">
            <div id="map-canvas" class="map-canvas" data-lat="40.748817" data-lng="-73.985428" style="height: 600px;"></div>
          </div>
        </div>
@endsection