<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    protected $table = 'posts';
    protected $fillable = [
        'id','title','content','status_ide', 'taken_by'
    ];
}
