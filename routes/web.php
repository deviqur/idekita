<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'admin','prefix' => 'admin'],function(){
    Route::get('/', 'Admin\UserController@index')->name('admin.user.index');
    Route::get('/users', 'Admin\UserController@index')->name('admin.user.index');
    Route::put('/users/updateStatus/{id}/{status_ide}', 'Admin\UserController@updateStatus')->name('admin.user.updateStatus');
    Route::delete('/users/destroy/{id}', 'Admin\UserController@destroy')->name('admin.user.destroy');


    Route::get('/ideas', 'Admin\IdeasController@index')->name('admin.ideas.index');
    Route::put('/ideas/updateStatusIde/{id}/{status}', 'Admin\IdeasController@updateStatusIde')->name('admin.ideas.updateStatusIde');
    Route::delete('/ideas/destroy/{id}', 'Admin\IdeasController@destroy')->name('admin.ideas.destroy');
    Route::post('/ideas/store/', 'Admin\IdeasController@store')->name('admin.ideas.store');

    Route::get('/comments', 'Admin\CommentsController@index')->name('admin.comments.index');
    Route::put('/comments/updateStatus/{id}/{status}', 'Admin\CommentsController@updateStatus')->name('admin.comments.updateStatus');
    Route::delete('/comments/destroy/{id}', 'Admin\CommentsController@destroy')->name('admin.comments.destroy');
    

});

Route::group(['middleware' => 'user','prefix' => 'user'],function(){
    Route::get('/', 'User\PostController@index')->name('user.post.dashboard');
    
});



Route::get('/', function() {
    return view('welcome');}
);


Route::get('createcaptcha', 'CaptchaController@create');
Route::post('captcha', 'CaptchaController@captchaValidate');
Route::get('refreshcaptcha', 'CaptchaController@refreshCaptcha');

Route::get('/home', 'HomeController@index')->name('home');

?>
